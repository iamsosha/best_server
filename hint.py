from PyQt5 import QtCore, QtGui, QtWidgets
import UI

class ExampleApp(QtWidgets.QMainWindow, UI.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

app = QtWidgets.QApplication([])
window = ExampleApp()
window.show()
app.exec_()