import requests
import time
import datetime

last_time = 0

while True:
    response = requests.get("http://127.0.0.1:5000/messages",
                            params={'after': last_time})
    messages = response.json()["messages"]

    print(1)
    for message in messages:
        print(2)
        good_time = datetime.datetime.fromtimestamp(message["time"])
        good_time = good_time.strftime('%d/%m/%Y %H:%M:%S')
        print(message["username"], good_time)
        print(message["text"])
        print()

        last_time = message["time"]

    time.sleep(1)
