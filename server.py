import datetime
import time
from flask import Flask, request, render_template

app = Flask(__name__)

users = {}

@app.route("/")
def hello_view():
    return "<h1>Hello, user!</h1>"


@app.route('/index')
def index():
    user = {'username': 'Sosha'}
    posts = [
        {
            'author': {'username': 'Sosha'},
            'body': 'Welcome to the server!'
        }
    ]
    return 0
 #    return render_template('index.html', title='Welcome to the chat!', user=user, posts=posts)


@app.route("/feedback")
def feedback():
    return 'Feedback Page'


@app.route("/status")
def status_viev():
    return {
        'status': True,
        'time': datetime.now().strftime('%d/%m/%Y %H:%M:%S'),
        'messeges_count': len(messages),
        'users_count': len(users)
    }


@app.route("/send", methods=['POST'])
def send_viev():
    data = request.json
    username = data["username"]
    password = data["password"]

    if username not in users or users[username] != password:
        return {"ok": False}

    text = data["text"]
    messages.append({"username": username, "text": text, "time": time.time()})

    return {'ok': True}


messages = []


@app.route("/messages")
def messages_viev():
    after = float(request.args['after'])
    new_messages = [message for message in messages if message['time'] > after]
    return {'messages': new_messages}


@app.route("/auth", methods=['POST'])
def auth_view():
    data = request.json
    username = data["username"]
    password = data["password"]

    if username not in users:
        users[username] = password
        return {"ok": True}
    elif users[username] == password:
        return {"ok": True}
    else:
        return {"ok": False}


if __name__ == "__main__":
    app.run()
